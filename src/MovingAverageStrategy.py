import logging
import pandas as pd
import numpy as np


def init(context):
    logging.debug("init")
    # register the data you are interested in
    context.ccy_pair = "EURUSD"
    context.freq = "1M"
    context.size = 30
    context.start_date = '1-05-2018'
    context.end_date = '10-05-2018'
    context.ordersCount = 0
    context.lastTrade = 0


# df is a numpy 2d array
def on_quote(context):
    df = context.data
    size = context.size
    trade_api = context.tradeapi
    last_ccy_pair = context.last_ccy_pair
    last_close = df['close'][size-1]
    t = np.arange(0, size)
    d = pd.Series(df['close'], t)
    mov_age_short = d.rolling(5).sum() / 5
    mov_age_long = d.rolling(25).sum() / 25
    deltas = mov_age_short - mov_age_long
    if deltas[size - 1] * deltas[size - 2] < 0:
        logging.debug("trade!")
        if deltas[size - 1] < 0 and context.lastTrade >= 0:
            if context.lastTrade != 0:
                trade_api.trade(last_ccy_pair, -2, last_close)
                context.ordersCount = context.ordersCount + 1
            trade_api.trade(last_ccy_pair, -2, last_close)
            context.ordersCount = context.ordersCount + 1
            context.lastTrade = -2
        elif deltas[size - 1] > 0 and context.lastTrade <= 0:
            if context.lastTrade != 0:
                trade_api.trade(last_ccy_pair, 2, last_close)
                context.ordersCount = context.ordersCount + 1
            trade_api.trade(last_ccy_pair, 2, last_close)
            context.ordersCount = context.ordersCount + 1
            context.lastTrade = 2


def on_results(context, resp):
    t = resp['type']
    if t == "RESULTS":
        logging.debug("result")
